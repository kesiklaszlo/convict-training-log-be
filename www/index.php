<?php

require __DIR__ . '/../vendor/autoload.php';
$settings = require __DIR__ . '/../config/settings.php';
$app = new \Slim\App($settings);

require __DIR__ . '/../config/routes.php';
require __DIR__ . '/../config/doctrine.php';

$app->run();


<?php
use Doctrine\ORM\Tools\Setup;
$container = $app->getContainer();

$container['entityManager'] = function ($c) {
    // Create a simple "default" Doctrine ORM configuration for Annotations
    $isDevMode = true;
    $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src"), $isDevMode);
    // or if you prefer yaml or XML
    // $config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
    // $config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);
    
    $connectionOptions = array(
        'driver' => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => '',
        'user' => '',
        'password' => ''
    );
    
    $entityManager = EntityManager::create($connectionOptions, $config);
    
    return $entityManager;
};



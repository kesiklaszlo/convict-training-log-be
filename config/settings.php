<?php
return [
    'settings' => [
        'status' => 'dev',
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true
        /*'db' => [
            'driver' => 'mysql',
            'host' => '',
            'database' => '',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
        ]*/
    ]
];
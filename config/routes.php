<?php
use hu\kesik\ctl\controller\UserController;

$app->get('/', function ($request, $response, $args) {
    echo 'Hello World!';
});

$app->get('/me', UserController::class . ':me');
<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="UserRepository") @Table(name="Users")
 */
class User {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $password;
    
    /**
     * @OneToMany(targetEntity="Training", mappedBy="user")
     * @var Training[]
     */
    protected $trainings;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }
}
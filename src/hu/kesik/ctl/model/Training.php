<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="TrainingRepository") @Table(name="Trainings")
 */
class Training {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="time")
     * @var time
     */
    protected $startTime;

    /**
     * @Column(type="time")
     * @var time
     */
    protected $endTime;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $comment;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="trainings")
     * @var User
     */
    protected $user;
    
    /**
     * @OneToMany(targetEntity="Repetition", mappedBy="training")
     * @var Repetition[]
     */
    protected $repetition;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getStartTime() {
        return $this->startTime;
    }

    public function setStartTime($startTime) {
        $this->startTime = $startTime;
        return $this;
    }

    public function getEndTime() {
        return $this->endTime;
    }

    public function setEndTime($endTime) {
        $this->endTime = $endTime;
        return $this;
    }

    public function getComment() {
        return $this->comment;
    }

    public function setComment($comment) {
        $this->comment = $comment;
        return $this;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
        return $this;
    }
}

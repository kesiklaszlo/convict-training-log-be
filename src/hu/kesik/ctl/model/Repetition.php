<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="RepetitionRepository") @Table(name="Repetitions")
 */
class Repetition {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;
    
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $sets;
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $reps;
    
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $seconds;
    
    /**
     * @ManyToOne(targetEntity="Exercise", inversedBy="repetitions")
     * @var Exercise
     */
    protected $exercise;
    
    /**
     * @ManyToOne(targetEntity="Training", inversedBy="repetitions")
     * @var Training
     */
    protected $training;

}


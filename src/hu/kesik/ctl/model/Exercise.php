<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="ExerciseRepository") @Table(name="Exercises")
 */
class Exercise {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $imageUrl;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $level;

    /**
     * @ManyToOne(targetEntity="ExerciseType", inversedBy="exercises")
     * @var ExerciseType
     */
    protected $exerciseType;
    
    /**
     * @OneToMany(targetEntity="Goal", mappedBy="exercise")
     * @var Goal[]
     */
    protected $goals;
    
    /**
     * @OneToMany(targetEntity="Repetition", mappedBy="exercise")
     * @var Repetition[]
     */
    protected $repetition;


    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getImageUrl() {
        return $this->imageUrl;
    }

    public function setImageUrl($imageUrl) {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    public function getLevel() {
        return $this->level;
    }

    public function setLevel($level) {
        $this->level = $level;
        return $this;
    }

    public function getExerciseType() {
        return $this->exerciseType;
    }

    public function setExerciseType($exerciseType) {
        $this->exerciseType = $exerciseType;
        return $this;
    }
}
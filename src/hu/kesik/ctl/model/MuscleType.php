<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="MuscleTypeRepository") @Table(name="MuscleTypes")
 */
class MuscleType {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $imageUrl;

    /**
     * @Column(type="text")
     * @var text
     */
    protected $description;

    /**
     * @ManyToMany(targetEntity="ExerciseType")
     * @JoinTable(name="MuscleType_ExerciseType")
     * @var ExerciseType[]
     */
    protected $exerciseTypes;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getImageUrl() {
        return $this->imageUrl;
    }

    public function setImageUrl($imageUrl) {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function getExerciseTypes() {
        return $this->exerciseTypes;
    }

    public function setExerciseTypes($exerciseTypes) {
        $this->exerciseTypes = $exerciseTypes;
        return $this;
    }
}
<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="GoalRepository") @Table(name="Goals")
 */
class Goal {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $hardness;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $sets;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $reps;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $seconds;

    /**
     * @ManyToOne(targetEntity="Exercise", inversedBy="goals")
     * @var Exercise
     */
    protected $exercise;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getHardness() {
        return $this->hardness;
    }

    public function setHardness($hardness) {
        $this->hardness = $hardness;
        return $this;
    }

    public function getSets() {
        return $this->sets;
    }

    public function setSets($sets) {
        $this->sets = $sets;
        return $this;
    }

    public function getReps() {
        return $this->reps;
    }

    public function setReps($reps) {
        $this->reps = $reps;
        return $this;
    }

    public function getSeconds() {
        return $this->seconds;
    }

    public function setSeconds($seconds) {
        $this->seconds = $seconds;
        return $this;
    }

    public function getExercise() {
        return $this->exercise;
    }

    public function setExercise($exercise) {
        $this->exercise = $exercise;
        return $this;
    }
}


<?php
namespace hu\kesik\ctl\model;

/**
 * @Entity(repositoryClass="ExerciseTypeRepository") @Table(name="ExerciseTypes")
 */
class ExerciseType {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $imageUrl;

    /**
     * @OneToMany(targetEntity="Exercise", mappedBy="exerciseType")
     * @var Exercise[]
     */
    protected $exercises;

    /**
     * @ManyToMany(targetEntity="MuscleType", mappedBy="exerciseTypes")
     * @var MuscleType[]
     */
    protected $muscleTypes;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getImageUrl() {
        return $this->imageUrl;
    }

    public function setImageUrl($imageUrl) {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    public function getExercises() {
        return $this->exercises;
    }

    public function setExercises($exercises) {
        $this->exercises = $exercises;
        return $this;
    }

    public function getMuscleTypes() {
        return $this->muscleTypes;
    }

    public function setMuscleTypes($muscleTypes) {
        $this->muscleTypes = $muscleTypes;
        return $this;
    }
}

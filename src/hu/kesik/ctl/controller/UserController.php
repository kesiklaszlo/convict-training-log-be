<?php
namespace hu\kesik\ctl\controller;

class UserController extends Controller
{

    public function me($request, $response, $args)
    {
        return $response->withJson('me');
    }
}